# ABay Project - Payment Service


run these commands to pull and run payment-service image from Docker hub:

Get image postgres:\
"docker pull postgres"

Run postgres with these properties:\
"docker run --name postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=abay-payment -d -p 5432:5432 postgres"

Link postgres with payment-service:1.0 and run it:\
"docker run --link postgres rauldocker970/payment-service:1.0"

\
\
docker-compose.yml:\
\
while inside the project folder:\
"docker-compose up"

The docker-compose.yml file has two services: "app" and "postgres".\
\
"app" builds and deals with the docker image of the project. It has the database credentials and the port 8080 specified.
\
"postgres" deals with the image postgres, also with the database credentials and name, and with the port 5432 specified.
\
The "app" service is linked to the "postgres" one, like it's shown in the file.

\
\
The CI/CD pipeline was implemented like this:

- a file called "docker-compose.prod.yml" was created, which is basically a copy of "docker-compose.yml" with these changes: the line "build ." was
removed; the image in the service "app" was changed to "esue1920/payment-service"; the port part of SPRING_DATASOURCE_URL in the environment of "app"
was changed to "5435"; the ports of "app" were changed to "8085:8080"; expose of the service "postgres" is now "5435", as well as the ports part 
which is now "5435:5435".

- the following environment variables were added to the settings of CI/CD of the project: "CI_REGISTRY", "CI_REGISTRY_USER", "CI_REGISTRY_PASSWORD",
"DB_NAME", "DB_PASSWORD", "DB_URL", "DB_USER", "SSH_PRIVATE_KEY". The values of the variables "SSH_PRIVATE_KEY" and "CI_REGISTRY" were taken from 
the "rest-jpa-test" project.

- the files "deploy.sh" and "setup_env.sh" were added and also taken from the "rest-jpa-test" project. The line "cd ./app" was changed to
"cd ./payment-service" in the "deploy.sh" file.

- ".gitlab-ci.yml" was created and also mostly taken from the "rest-jpa-test" project. In the script part of the deploy stage, the end of the first
and second lines were changed from "./app" to "./payment-service".
