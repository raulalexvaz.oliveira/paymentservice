package payment.requests;

import java.math.BigDecimal;

public class ChargeWalletRequest {
    private String username;
    private BigDecimal value;

    public ChargeWalletRequest(){
    }

    public ChargeWalletRequest(String username, BigDecimal value){
        this.username = username;
        this.value = value;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public BigDecimal getWalletValue() {
        return value;
    }

    public void setWalletValue(BigDecimal value) {
        this.value = value;
    }
}

