package payment.requests;

public class PaymentRequest {
    private String username;

    public PaymentRequest(){
    }

    public PaymentRequest(String username){
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
