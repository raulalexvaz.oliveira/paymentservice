package payment.requests;

public class NewWalletRequest {
    private String username;

    public NewWalletRequest(){
    }

    public NewWalletRequest(String username){
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
