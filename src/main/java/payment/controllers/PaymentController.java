package payment.controllers;

import org.springframework.stereotype.Controller;
import payment.requests.*;
import payment.responses.*;
import payment.services.PaymentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Controller
public class PaymentController {

    final PaymentManager paymentManager;

    @Autowired
    public PaymentController(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    @PostMapping(value = "/shoppingCart/payment")
    public @ResponseBody
    PaymentResponse handlePayment(@RequestBody PaymentRequest paymentRequest) {

        String username = paymentRequest.getUsername();

        if (username != null) {
            if (paymentManager.paymentSuccess(username)) {
                return new PaymentResponse(true);
            } else {
                return new PaymentResponse(false, "User doesn't have enough credit.");
            }
        }

        return new PaymentResponse(false, "User does not exist.");
    }

    @PostMapping(value = "/newWallet")
    public @ResponseBody
    NewWalletResponse handleNewWallet(@RequestBody NewWalletRequest newWalletRequest) {

        String username = newWalletRequest.getUsername();

        if (username != null) {
            BigDecimal val = paymentManager.checkCredit(username);

            if (val == null) {
                boolean bool = paymentManager.updateUserWallet(username, BigDecimal.valueOf(0));

                if (bool) {
                    return new NewWalletResponse(true);
                } else {
                    return new NewWalletResponse(false, "User does not exist.");
                }
            } else {
                return new NewWalletResponse(false, "User already has a wallet.");
            }
        }

        return new NewWalletResponse(false, "User does not exist.");
    }

    @PostMapping(value = "/chargeWallet")
    public @ResponseBody
    ChargeWalletResponse handleChargeWallet(@RequestBody ChargeWalletRequest chargeWalletRequest) {

        //create and save a user for testing
        paymentManager.newUser("Test", "test@gmail.com", "test123", "pass123", BigDecimal.TEN, false);

        String username = chargeWalletRequest.getUsername();
        BigDecimal value = chargeWalletRequest.getWalletValue();

        if (username != null && value != null) {

            BigDecimal currentWallet = paymentManager.checkCredit(username);

            if(currentWallet != null) {
                BigDecimal newWallet = currentWallet.add(value);

                System.out.println("currentWallet:");
                System.out.println(currentWallet);
                System.out.println("value to add:");
                System.out.println(value);

                //this should output 60 (10 (initial wallet value) + 50 (value added))
                System.out.println("newWallet:");
                System.out.println(newWallet);

                if(paymentManager.updateUserWallet(username, newWallet)) {
                    return new ChargeWalletResponse(true);
                }
            } else {
                return new ChargeWalletResponse(false, "User does not have a wallet.");
            }
        }

        return new ChargeWalletResponse(false, "Something is wrong, check if you have enough balance.");
    }
}
