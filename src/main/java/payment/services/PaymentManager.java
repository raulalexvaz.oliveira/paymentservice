package payment.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import payment.entities.UserEntity;
import payment.entities.Cart;
import payment.repositories.UserRepository;
import payment.repositories.CartRepository;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class PaymentManager
{
    final UserRepository userRepository;
    final CartRepository cartRepository;

    @Autowired
    public PaymentManager(UserRepository userRepository, CartRepository cartRepository){
        this.userRepository = userRepository;
        this.cartRepository = cartRepository;
    }

    public boolean paymentSuccess(String username)
    {
        Optional<UserEntity> u = userRepository.findById(username);
        UserEntity user = u.orElse(null);
        Cart cart = cartRepository.findByUsername(username);

        if(user != null) {
            if (cart.getUsername().equals(user.getName())) {
                if (user.getWallet().compareTo(cart.getTotalCost()) >= 0)
                    return true;
                else
                    return false;
            }
        }

        return false;
    }

    public boolean userNotEnoughCredit(UserEntity userEntity)
    {
        Cart cart = cartRepository.findByUsername(userEntity.getUsername());

        if(cart != null) {
            if (cart.getUsername().equals(userEntity.getName())) {
                return userEntity.getWallet().compareTo(cart.getTotalCost()) < 0;
            }
        }

        return true;
    }

    public BigDecimal checkCredit(String username)
    {
        Optional<UserEntity> u = userRepository.findById(username);
        UserEntity user = u.orElse(null);

        if(user != null)
            return user.getWallet();

        return BigDecimal.ZERO;
    }

    public boolean updateUserWallet(String username, BigDecimal value)
    {
        Optional<UserEntity> u = userRepository.findById(username);
        UserEntity user = u.orElse(null);

        if(user != null){
            user.setWallet(value);
            userRepository.save(user);

            return true;
        }

        return false;
    }

    public void newUser(String name, String email, String username, String password, BigDecimal initialWallet, boolean isAdmin) {
        UserEntity user = new UserEntity(name, email, username, password, initialWallet, isAdmin, null, 0);
        userRepository.save(user);
    }
}