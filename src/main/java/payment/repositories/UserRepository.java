package payment.repositories;

import payment.entities.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {

    boolean existsUserEntityByEmail(String email);

    @Query("SELECT u.username FROM UserEntity u")
    Collection<String> getAllUsernames();

}