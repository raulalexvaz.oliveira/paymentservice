package payment.repositories;

import org.springframework.stereotype.Repository;
import payment.entities.Cart;
import org.springframework.data.repository.CrudRepository;

@Repository
public interface CartRepository extends CrudRepository<Cart, Integer> {

    Cart findByUsername(String username);

}
