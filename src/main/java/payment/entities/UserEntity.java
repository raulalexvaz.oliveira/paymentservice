package payment.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class UserEntity {
    @Id
    String username;
    String name;
    String email;
    String password;
    BigDecimal wallet;
    boolean isAdmin;
    Date suspendedUntil;
    int infractions;

    protected UserEntity() {
    }

    public UserEntity(String Name, String Email, String Username, String Password, BigDecimal Wallet, boolean isAdmin, Date suspendedUntil, int infractions) {
        this.name = Name;
        this.email = Email;
        this.username = Username;
        this.wallet = Wallet;
        this.password = Password;
        this.isAdmin = isAdmin;
        this.suspendedUntil = suspendedUntil;
        this.infractions = infractions;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getWallet() {
        return wallet;
    }

    public void setWallet(BigDecimal wallet) {
        this.wallet = wallet;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Date getSuspendedUntil() {
        return suspendedUntil;
    }

    public void setSuspendedUntil(Date suspendedUntil) {
        this.suspendedUntil = suspendedUntil;
    }

    public int getInfractions() {
        return infractions;
    }

    public void setInfractions(int infractions) {
        this.infractions = infractions;
    }
}