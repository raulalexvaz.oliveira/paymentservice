package payment.responses;

public class NewWalletResponse {

    private Boolean status;
    private String message;

    public NewWalletResponse(){
    }

    public NewWalletResponse(Boolean status) {
        this.status = status;
    }

    public NewWalletResponse(Boolean status, String message) {
        this.status = status;
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}