package payment.SpringApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;
import payment.requests.ChargeWalletRequest;
import payment.responses.ChargeWalletResponse;

import java.math.BigDecimal;

@SpringBootApplication
@EntityScan(basePackages = "payment")
@ComponentScan(basePackages = "payment")
@EnableJpaRepositories("payment.repositories")
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);

        String url = "http://localhost:8080/chargeWallet";

        //user is created on "PaymentController", in handleChargeWallet with value of 10 in wallet
        String username = "test123";
        BigDecimal val = BigDecimal.valueOf(50);

        //charge wallet with "val" which is 50
        ChargeWalletRequest chargeWalletReq = new ChargeWalletRequest(username, val);

        RestTemplate temp = new RestTemplate();

        ChargeWalletResponse result = temp.postForObject(url, chargeWalletReq, ChargeWalletResponse.class);

        boolean stat;
        String mess = "";

        if(result != null){
            stat = result.getStatus();
            mess = result.getMessage();
        } else {
            stat = false;
        }

        System.out.println(stat);
        
        if (mess != null) {
            System.out.println(mess);
        }
    }
}