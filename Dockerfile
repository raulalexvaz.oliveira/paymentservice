FROM openjdk:8-jdk-alpine

WORKDIR /

ADD target/payment-service-1.0-SNAPSHOT.jar payment-service.jar

CMD java -jar payment-service.jar